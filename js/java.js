// $(document).ready(polizas());
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');

    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();

        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);




})();

$(function(){

$('#validationCustom05').on('keyup',function(){
		var pw1	 = $('#validationCustom03').val();
		var pw2 = $('#validationCustom05').val();
	
			if (pw1===pw2) {
				$('#validationCustom05').removeClass('is-invalid');
				$('#validationCustom05').addClass('is-valid');


				$('#guardar').removeAttr('disabled', '');
				
			}else{
				$('#validationCustom05').removeClass('is-valid');
				$('#validationCustom05').addClass('is-invalid');

				$('#guardar').attr('disabled', '');
	
			}

	return false;
	});
$('#validationCustom03').on('keyup',function(){
		var pw1	 = $('#validationCustom03').val();
		var pw2 = $('#validationCustom05').val();
	
			if (pw1===pw2) {
				$('#validationCustom05').removeClass('is-invalid');
				$('#validationCustom05').addClass('is-valid');


				$('#guardar').removeAttr('disabled', '');
				
			}else{
				$('#validationCustom05').removeClass('is-valid');
				$('#validationCustom05').addClass('is-invalid');

				$('#guardar').attr('disabled', '');
	
			}

	return false;
	});
$('#validationCustomUsername').on('keyup',function(){
		var user = $('#validationCustomUsername').val();
		var url = 'validaUsuario.php';

	$.ajax({
		url:url,
		type:'POST',
		data:'usuario='+user,
		success:function(data){

			if (data == 0) {
				$('#validationCustomUsername').removeClass('is-invalid');
				$('#validationCustomUsername').addClass('is-valid');


				$('#guardar').removeAttr('disabled', '');
			}else{
				$('#validationCustomUsername').removeClass('is-valid');
				$('#validationCustomUsername').addClass('is-invalid');

				$('#guardar').attr('disabled', '');
		       var i = Number(data); 
				switch(i) {
  				case 1:
 					$('#msguser').html('<p>!Usuario vacio</p>');
 					
    			break;
  				case 2:
  					$('#msguser').html('<p>!Nombre de usuario debe tener mas de 5 caracteres</p>');
  					
        		break;
  				case 3:
  					$('#msguser').html('<p>!Nombre de usuario ya existe</p>');
    			break;
  				default:
                $('#msguser').html('<p>!Usuario no permitido</p>');
                break;
				}
			}
		}
	});
	return false;
	});


});


function BuscarUso(){
var url = 'listaUsos.php';
var uso = $('#ibuscar').val();
	$.ajax({
		url:url,
		type:'POST',
		data:'uso='+uso,
		success:function(data){
			$('#resultados').html(data);
		}
	});

	return false;
}
function BuscarUsuario(){
var url = 'listaUsuarios.php';
var uso = $('#ibuscar').val();
	$.ajax({
		url:url,
		type:'POST',
		data:'uso='+uso,
		success:function(data){
			$('#resultados').html(data);
		}
	});

	return false;
}
function VerSuturas(id){
	var id  = id;
	window.location='VerSuturas.php?id='+id;

}
function BuscarSutura(){
var url = 'listaSutura.php';
var uso = $('#ibuscar').val();
	$.ajax({
		url:url,
		type:'POST',
		data:'uso='+uso,
		success:function(data){
			$('#resultado').html(data);
		}
	});

	return false;
}
function VerSuturas1(id){
	var id  = id;
	window.location='versutura1.php?id='+id;

}


function eliminarUsuario(id){

alertify.confirm('Eliminar', 'Estas seguro que desea de eliminar este usuario?', 
	function(){ 
var caso = 1;
var url = 'borrarUsuario.php';
	$.ajax({
		url:url,
		type:'POST',
		data:'id='+id+'&caso='+caso,
		success:function(data){
			
			var i = Number(data); 

			switch (i){

				case 1:
				alertify.success('Eliminado');
				$('#labelerror').attr('hidden','true');
				BuscarUsuario();
				break;
				default:
				$('#labelerror').html(data);
				$('#labelerror').removeAttr('hidden');
				alertify.error('Cancelado');
				break;

			}
		}
	});

		 }, 
	function(){ 


		alertify.error('Cancelado')});

}

function reiniciarclave(id){
alertify.set('notifier','position', 'top-center');
alertify.prompt( 'Reiniciar contraseña', 'Nueva contraseña', '123456'
               , function(evt, value) { 
var caso = 2;
var url = 'borrarUsuario.php';
               		$.ajax({
		url:url,
		type:'POST',
		data:'id='+id+'&caso='+caso+'&pass='+value,
		success:function(data){		
			var i = Number(data); 
			switch (i){

				case 1:
				alertify.success('contraseña reiniciada');
				$('#labelerror').attr('hidden','true');
				BuscarUsuario();
				break;
				default:
				$('#labelerror').html(data);
				$('#labelerror').removeAttr('hidden');
				alertify.error('Error actualizando');
				break;

			}
		}
	});

 }
               , function() {
                alertify.error('Cancelado') }).set('type', 'password');
}


function eliminar(id){
alertify.set('notifier','position', 'top-center');
	alertify.confirm('Eliminar', 'Estas seguro que desea de eliminar esta sutura?', 
	function(){ 
var caso = 1;
var url = 'borrar.php';
	$.ajax({
		url:url,
		type:'POST',
		data:'id='+id+'&caso='+caso,
		success:function(data){
			
			var i = Number(data); 

			switch (i){

				case 1:
				alertify.success('Eliminada');
				$('#labelerror').attr('hidden','true');
				BuscarSutura();
				break;
				default:
				$('#labelerror').html(data);
				$('#labelerror').removeAttr('hidden');
				alertify.error('Error');
				break;

			}
		}
	});

		 }, 
	function(){ 
		alertify.error('Cancelado')});

}

function eliminaruso(id){
alertify.set('notifier','position', 'top-center');
	alertify.confirm('Eliminar', 'Estas seguro que desea de eliminar esta sutura?', 
	function(){ 
var caso = 2;
var url = 'borrar.php';
	$.ajax({
		url:url,
		type:'POST',
		data:'id='+id+'&caso='+caso,
		success:function(data){
			
			var i = Number(data); 

			switch (i){

				case 1:
				alertify.success('Eliminado');
				$('#labelerror').attr('hidden','true');
				BuscarUso();
				break;
				default:
				$('#labelerror').html(data);
				$('#labelerror').removeAttr('hidden');
				alertify.error('Error');
				break;

			}
		}
	});

		 }, 
	function(){ 
		alertify.error('Cancelado')});

}