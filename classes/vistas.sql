CREATE OR REPLACE VIEW
vista_suturas AS 
SELECT suturas.id_sutura, suturas.nombre, suturas.codigo,suturas.marca,usos.titulo FROM suturas
LEFT JOIN usos 
ON suturas.uso = usos.id_uso ;