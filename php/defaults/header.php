<?php $url = 'http://localhost/SCGS/php/'; ?>
<html lang="es">
  <head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Sistema de Suturas</title>

    <!-- Bootstrap core CSS -->
 <link href="../../css/bootstrap.min.css" rel="stylesheet">
 <link href="../../css/mycss.css" rel="stylesheet">
 <link href="../../css/all.min.css" rel="stylesheet">
 <link href="../../css/alertify.min.css" rel="stylesheet">

    
  
  </head>
<body>
<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
  <div class="shadow bg-gris rounded">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href=" <?php echo $url; ?>">Suturas app</a>
  </div>

        <ul class="navbar-nav col">
        <li class="nav-item text-nowrap">
          <a class="navbar-brand  btn-outline-secondary rounded bm" href=" <?php echo $url; ?>sutura ">Buscar&nbsp;<i class="fas fa-search"></i></a>
          <?php 
          if ($_SESSION['nivel'] == 1) { ?>
            <a class="navbar-brand  btn-outline-secondary rounded bm" href=" <?php echo $url; ?>sutura/Suturas.php ">Suturas&nbsp;<i class="fas fa-archive"></i></a>
          <a class="navbar-brand  btn-outline-secondary rounded bm" href=" <?php echo $url; ?>usuarios ">Usuarios&nbsp;<i class="fas fa-user-friends"></i></a>
          
          <?php } ?>
         
        
        </li>

      </ul>
  
  
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="../../index.php?logout"><span class="icon-exit"></span>&nbsp; Salir</a>
        </li>

      </ul>
    </nav>
    