<?php 
session_start();
	if (!isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] != 1) {
        header("location:../");
		exit;
        }
include("../defaults/conexion.php");
include("../defaults/header.php");
$id = $_REQUEST['id'];
$registro = mysqli_query($enlace,"SELECT *FROM usos where id_uso = '$id'");
$registro2 = mysqli_fetch_array($registro);
$titulo = $registro2['titulo'];
$uso = $registro2['uso'];

$registro = mysqli_query($enlace,"SELECT suturas.nombre,suturas.codigo,suturas.especificacion,suturas.aguja,suturas.longitud,suturas.hebra,suturas.tamano,suturas.unidades,suturas.color,suturas.img,marcas.nombre as 'marca' FROM `suturas` INNER JOIN marcas on suturas.marca = marcas.id where uso = '$id'");
$i = 1;
$su1 = false;
$su2 = false;
if(mysqli_num_rows($registro)>0){
	while($registro2 = mysqli_fetch_array($registro)){

		switch ($i) {
			case 1:
			$sutura1 = [
				'nombre' => $registro2['nombre'],
				'codigo' => $registro2['codigo'],
				'especifcacion' => $registro2['especificacion'],
				'aguja' => $registro2['aguja'],
				'longitud' => $registro2['longitud'],
				'hebra' => $registro2['hebra'],
				'tamano' => $registro2['tamano'],
				'unidades' => $registro2['unidades'],
				'color' => $registro2['color'],
				'marca' => $registro2['marca'],
				'img' => $registro2['img']
			];
				$i++;
				$su1 =true;
				break;
			case 2:
							$sutura2 = [
				'nombre' => $registro2['nombre'],
				'codigo' => $registro2['codigo'],
				'especifcacion' => $registro2['especificacion'],
				'aguja' => $registro2['aguja'],
				'longitud' => $registro2['longitud'],
				'hebra' => $registro2['hebra'],
				'tamano' => $registro2['tamano'],
				'unidades' => $registro2['unidades'],
				'color' => $registro2['color'],
				'marca' => $registro2['marca'],
				'img' => $registro2['img'],];
				$i++;
				$su2 =true;
				break;
			default:
		}


  }

if (!isset($sutura1)) {
				$sutura1 = [
				'nombre' => 'NO EXISTE',
				'codigo' => 'NO EXISTE',
				'especifcacion' => 'NO EXISTE',
				'aguja' => 'NO EXISTE',
				'longitud' => 'NO EXISTE',
				'hebra' => 'NO EXISTE',
				'tamano' => 'NO EXISTE',
				'unidades' => 'NO EXISTE',
				'color' => 'NO EXISTE',
				'marca' => 'NO EXISTE',
				'img' => 'NO EXISTE'
			];
}
if (!isset($sutura2)) {
										$sutura2 = [
				'nombre' => 'NO EXISTE',
				'codigo' => 'NO EXISTE',
				'especifcacion' => 'NO EXISTE',
				'aguja' => 'NO EXISTE',
				'longitud' => 'NO EXISTE',
				'hebra' => 'NO EXISTE',
				'tamano' => 'NO EXISTE',
				'unidades' => 'NO EXISTE',
				'color' => 'NO EXISTE',
				'marca' => 'NO EXISTE',
				'img' => '../../img/sinimagen.jpg'
			];
}

}
?>

<div class="container detalle border border-secondary rounded">
	<center>
		<h1><?php echo $titulo; ?></h1>
	</center>
	<div class="row"  style="margin-top: 10px;" >

		<div class="col card rounded sutura" >
<figure class="figure">
  	<img src=" <?php echo $sutura1['img']; ?> " class="figure-img img-fluid rounded" alt="...">
  	<h4 ><?php echo $sutura1['nombre']; ?></h4>
  	<h4 class="figure-caption">Codigo:&nbsp;<?php echo $sutura1['codigo']; ?></h4>
  	<h4 class="figure-caption">Especificación:&nbsp;<?php echo $sutura1['especifcacion']; ?></h4>
  	<h4 class="figure-caption">Aguja:&nbsp;<?php echo $sutura1['aguja']; ?></h4>
  	<h4 class="figure-caption">Longitud:&nbsp;<?php echo $sutura1['longitud']; ?></h4>
  	<h4 class="figure-caption">Hebras:&nbsp;<?php echo $sutura1['hebra']; ?></h4>
  	<h4 class="figure-caption">tamaños:&nbsp;<?php echo $sutura1['tamano']; ?></h4>
  	<h4 class="figure-caption">Unidades:&nbsp;<?php echo $sutura1['unidades']; ?></h4>
  	<h4 class="figure-caption">Color:&nbsp;<?php echo $sutura1['color']; ?></h4>
  	<h4 class="figure-caption">Marca:&nbsp;<?php echo $sutura1['marca']; ?></h4>
</figure>
		</div>
		<div class="col">
			<figure class="figure">
  <!-- <img src="../../img/logo.png" class="figure-img img-fluid rounded" alt="..."> -->
  <h6 ><p><?php  echo $uso; ?></p>
</h6>
</figure>
		</div>
		<div class="col card rounded sutura" >
<figure class="figure">
  	<img src=" <?php echo $sutura2['img']; ?> " class="figure-img img-fluid rounded" alt="...">
  	<h4 ><?php echo $sutura2['nombre']; ?></h4>
  	<h4 class="figure-caption">Codigo:&nbsp;<?php echo $sutura2['codigo']; ?></h4>
  	<h4 class="figure-caption">Especificación:&nbsp;<?php echo $sutura2['especifcacion']; ?></h4>
  	<h4 class="figure-caption">Aguja:&nbsp;<?php echo $sutura2['aguja']; ?></h4>
  	<h4 class="figure-caption">Longitud:&nbsp;<?php echo $sutura2['longitud']; ?></h4>
  	<h4 class="figure-caption">Hebras:&nbsp;<?php echo $sutura2['hebra']; ?></h4>
  	<h4 class="figure-caption">tamaños:&nbsp;<?php echo $sutura2['tamano']; ?></h4>
  	<h4 class="figure-caption">Unidades:&nbsp;<?php echo $sutura2['unidades']; ?></h4>
  	<h4 class="figure-caption">Color:&nbsp;<?php echo $sutura2['color']; ?></h4>
  	<h4 class="figure-caption">Marca:&nbsp;<?php echo $sutura2['marca']; ?></h4>
</figure>
		</div>
		
	</div>
</div>



<?php
include("../defaults/menu.php");
 ?>