<?php 
session_start();
  if (!isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] != 1 AND $_SESSION['nivel'] !=1 ) {
        header("location:../");
        exit;
        }
include("../defaults/conexion.php");
include("../defaults/header.php");
if (isset($_POST["nombre"])) {
$nombre = $_REQUEST['nombre'];
$marca = $_REQUEST['marca'];
$codigo = $_REQUEST['codigo'];
$especificacion = $_REQUEST['especificacion'];
$aguja = $_REQUEST['aguja'];
$LONGITUD = $_REQUEST['LONGITUD'];
$HEBRA = $_REQUEST['HEBRA'];
$tamano = $_REQUEST['tamano'];
$unidades = $_REQUEST['unidades'];
$color = $_REQUEST['color'];

$valida = mysqli_query($enlace,"SELECT * FRom suturas where nombre = '$nombre' ");
$valida = mysqli_num_rows($valida);
if ($valida > 0) {
  $mensaje = 'Ya Existe una sutura con ese nombre';
}else{
if ($_FILES["archivo"]["tmp_name"]) {

    if($_FILES["archivo"]["error"]>0){
        echo "Error al cargar archivo"; 
        } else {
        
        $permitidos = array("image/jpeg","image/jpg","image/gif","image/png","application/pdf");
        $limite_kb = 500;
        
            
            $ruta = '../../img/';
            $archivo = $ruta.$_FILES["archivo"]["name"];
            $nombre1 = $_FILES["archivo"]["name"];
            if(!file_exists($ruta)){
                mkdir($ruta);
            }
            
            if(!file_exists($archivo)){
                
                $resultado = @move_uploaded_file($_FILES["archivo"]["tmp_name"], $archivo);
                 if ($resultado) {    
                $query = mysqli_query($enlace,"INSERT INTO suturas (nombre,marca,codigo,especificacion,aguja,longitud,hebra,tamano,unidades,img,uso,color)VALUES('$nombre','$marca','$codigo','$especificacion','$aguja','$LONGITUD','$HEBRA','$tamano','$unidades','$archivo',2,'$color')");
                if ($query) {
                  $mensajeS = "Sutura creada de manera exitosa";
                }else{
                  $mensaje = "No se pudo completar el registro";
                }

            }
           
            }else{
              $query = mysqli_query($enlace,"INSERT INTO suturas (nombre,marca,codigo,especificacion,aguja,longitud,hebra,tamano,unidades,img,uso,color)VALUES('$nombre','$marca','$codigo','$especificacion','$aguja','$LONGITUD','$HEBRA','$tamano','$unidades','$archivo',2,'$color')");
                              if ($query) {
                  $mensajeS = "Sutura creada de manera exitosa";
                }else{
                  $mensaje = "No se pudo completar el registro";
                }


            }
        
        
    }

  }else{
     $query = mysqli_query($enlace,"INSERT INTO suturas (nombre,marca,codigo,especificacion,aguja,longitud,hebra,tamano,unidades,img,uso,color)VALUES('$nombre','$marca','$codigo','$especificacion','$aguja','$LONGITUD','$HEBRA','$tamano','$unidades','../../img/sinimagen.jpg',2,'$color')");
                              if ($query) {
                  $mensajeS = "Sutura creada de manera exitosa";
                }else{
                  $mensaje = "No se pudo completar el registro";
                }
  }

}



}
?>

<div class="container detalle border border-secondary rounded" style="margin-top: 10px;">
<center><h1>Buscar suturas </h1></center>
<br>
<div class="input-group mb-3">
  <input type="text" class="form-control" placeholder="Buscar Sutura" aria-label="Buscar Sutura" aria-describedby="button-addon2" id="ibuscar">
  <div class="input-group-append">
    <button class="btn btn-outline-success" type="button" id="bbuscar" onclick="BuscarSutura();">Buscar</button>
  </div>
</div>
  <div class="alert alert-danger alert-dismissible" id="labelerror" role="alert" hidden="true">
          
  </div>
<?php 
if (isset($mensaje)) {?>
  <div class="alert alert-danger alert-dismissible" role="alert">
                <strong>Error!</strong> 
            <?php 
            echo $mensaje;
            ?>
  </div>
  <?php 
}

 ?>
 <?php 
if (isset($mensajeS)) {?>
  <div class="alert alert-success alert-dismissible" role="alert">
                <strong>Exito!</strong> 
            <?php 
            echo $mensajeS;
            ?>
  </div>
  <?php 
}

 ?>
<button type="button" class="btn btn-success mb-3" data-toggle="modal" data-target=".bd-example-modal-xl">Registrar Sutura &nbsp; <i class="fas fa-plus"></i></button>
<table class="table table-striped text-center">
<thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nombre</th>
      <th scope="col">Marca</th>
      <th scope="col">Codigo</th>
      <th scope="col">Uso</th>
      <th scope="col">Opciones</th>
    </tr>
 </thead>
<tbody id="resultado">

</tbody> 
  
</table>








</div>



<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
          <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Registrar Sutura</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
  <form method="POST" action="Suturas.php" enctype="multipart/form-data">
       <div class="modal-body">

  <div class="form-row">
    <div class="form-group col">
      <label for="nombre">NOMBRE</label>
      <input type="text" class="form-control" name="nombre" placeholder="NOMBRE" required>
    </div>
    <div class="form-group col">
  <label for="inputState">Sutura J&J</label>
      <select id="inputState"  name="marca" class="form-control">
        <option value="1">J&J</option>
        <option value="2">Hirsh</option>
      </select>
    </div>
    <div class="form-group col">
      <label for="codigo">CODIGO</label>
      <input type="text" class="form-control" name="codigo" placeholder="CODIGO" required>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col">
      <label for="especificacion">ESPECIFICACIÓN</label>
      <textarea class="form-control" aria-label="especificacion" name="especificacion" required></textarea>
    </div>
    <div class="form-group col">
      <label for="aguja">AGUJA</label>
      <input type="text" class="form-control" name="aguja" placeholder="AGUJA" required>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-2">
      <label for="LONGITUD">LONGITUD</label>
      <input type="number" class="form-control" name="LONGITUD" required>
    </div>
    <div class="form-group col-md-2">
      <label for="HEBRA">HEBRA</label>
       <input type="number" class="form-control" name="HEBRA" required>
    
    </div>
    <div class="form-group col-md-2">
      <label for="tamano">TAMAÑO</label>
      <input type="text" class="form-control" name="tamano" required>
    </div>
    <div class="form-group col-md-2">
      <label for="unidades">UNIDADES</label>
      <input type="number" class="form-control" name="unidades" required>
    </div>
          <div class="form-group col-2">
      <label for="color">COLOR</label>
      <input type="text" class="form-control" name="color" required>
    </div>
  </div>

    <div class="form-row">

   <div class="custom-file col">
      <input type="file" class="custom-file-input" id="customFile"  name="archivo">
       <label class="custom-file-label" for="customFile">Cargar imagen</label>
    </div>

  </div>
</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
      </div>
</form>
  
    </div>
  
</div>
</div>


<?php
include("../defaults/menu.php");
 ?>