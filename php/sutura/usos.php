<?php 
session_start();
  if (!isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] != 1) {
        header("location:../");
    exit;
        }
include("../defaults/conexion.php");
include("../defaults/header.php");
?>

<div class="container detalle border border-secondary rounded" style="margin-top: 10px;">
<center><h1>Buscar Usos</h1></center>
<br>
<div class="input-group mb-3">
  <input type="text" class="form-control" placeholder="Buscar Sutura" aria-label="Buscar Sutura" aria-describedby="button-addon2" id="ibuscar">
  <div class="input-group-append">
    <button class="btn btn-outline-success" type="button" id="bbuscar" onclick="BuscarUso();">Buscar</button>
  </div>
</div>
<button type="button" class="btn btn-success mb-3" data-toggle="modal" data-target=".bd-example-modal-xl">Registrar Sutura &nbsp; <i class="fas fa-plus"></i></button>
<table class="table table-striped">
<thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nombre</th>
      <th scope="col">Marca</th>
      <th scope="col">Codigo</th>
      <th scope="col">Uso</th>
      <th scope="col">Opciones</th>
    </tr>
 </thead>
<tbody id="resultado">

</tbody> 
  
</table>








</div>



<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
          <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Registrar Sutura</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
  <form method="POST" action="Suturas.php" enctype="multipart/form-data">
       <div class="modal-body">

  <div class="form-row">
    <div class="form-group col">
      <label for="nombre">NOMBRE</label>
      <input type="text" class="form-control" name="nombre" placeholder="NOMBRE" required>
    </div>
    <div class="form-group col">
      <label for="marca">MARCA</label>
      <input type="text" class="form-control" name="marca" placeholder="MARCA" required>
    </div>
    <div class="form-group col">
      <label for="codigo">CODIGO</label>
      <input type="text" class="form-control" name="codigo" placeholder="CODIGO" required>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col">
      <label for="especificacion">ESPECIFICACIÓN</label>
      <textarea class="form-control" aria-label="especificacion" name="especificacion" required></textarea>
    </div>
    <div class="form-group col">
      <label for="aguja">AGUJA</label>
      <input type="text" class="form-control" name="aguja" placeholder="AGUJA" required>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-3">
      <label for="LONGITUD">LONGITUD</label>
      <input type="number" class="form-control" name="LONGITUD" required>
    </div>
    <div class="form-group col-md-3">
      <label for="HEBRA">HEBRA</label>
       <input type="number" class="form-control" name="HEBRA" required>
    
    </div>
    <div class="form-group col-md-3">
      <label for="tamano">TAMAÑO</label>
      <input type="text" class="form-control" name="tamano" required>
    </div>
    <div class="form-group col-md-3">
      <label for="unidades">UNIDADES</label>
      <input type="number" class="form-control" name="unidades" required>
    </div>
  </div>

    <div class="form-row">
   <div class="custom-file ">
      <input type="file" class="custom-file-input" id="customFile"  name="archivo">
       <label class="custom-file-label" for="customFile">Cargar imagen</label>
    </div>
    
  </div>
</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
      </div>
</form>
  
    </div>
  
</div>
</div>


<?php
include("../defaults/menu.php");
 ?>