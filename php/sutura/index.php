<?php 
session_start();
  if (!isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] != 1) {
        header("location:../");
    exit;
        }
include("../defaults/conexion.php");
include("../defaults/header.php");
if (isset($_POST["titulo"])) {
  if (!isset($_POST["sutura1"])) {
    $mensaje = "DEBE AGREGAR UNA SUTURA J&J";
  }elseif (!isset($_POST["sutura2"])) {
  $mensaje = "DEBE AGREGAR UNA SUTURA Hirsh";
  }else{
$titulo = $_REQUEST['titulo'];
$uso = $_REQUEST['uso'];
$sutura1 = $_REQUEST['sutura1'];
$sutura2 = $_REQUEST['sutura2'];
 
$query = mysqli_query($enlace,"INSERT INTO usos (titulo,uso,sutura1,sutura2)VALUES('$titulo','$uso','$sutura1','$sutura2')");
$consulta = mysqli_query($enlace,"SELECT * from usos order by id_uso  DESC LIMIT 1 ");
$uso = mysqli_fetch_array($consulta);
$uso = $uso['id_uso'];
$query = mysqli_query($enlace,"UPDATE suturas SET uso = '$uso' where id_sutura = '$sutura1'");
$query = mysqli_query($enlace,"UPDATE suturas SET uso = '$uso' where id_sutura = '$sutura2'");
 $mensajeS = "USO CREADO DE MANERA EXITOSA";
  }

}
?>

<div class="container detalle border border-secondary rounded" style="margin-top: 10px;">
<center><h1>Buscar suturas por su uso </h1></center>
<br>
<div class="input-group mb-3">
  <input type="text" class="form-control" placeholder="Buscar Sutura" aria-label="Buscar Sutura" aria-describedby="button-addon2" id="ibuscar">
  <div class="input-group-append">
    <button class="btn btn-outline-success" type="button" id="bbuscar" onclick="BuscarUso();">Buscar</button>
  </div>
</div>
  <div class="alert alert-danger alert-dismissible" id="labelerror" role="alert" hidden="true">
          
  </div>
<?php 
if (isset($mensaje)) {?>
  <div class="alert alert-danger alert-dismissible" role="alert">
                <strong>Error!</strong> 
            <?php 
            echo $mensaje;
            ?>
  </div>
  <?php 
}

 ?>
 <?php 
if (isset($mensajeS)) {?>
  <div class="alert alert-success alert-dismissible" role="alert">
                <strong>Exito!</strong> 
            <?php 
            echo $mensajeS;
            ?>
  </div>
  <?php 
}

 ?>
 
  <?php 
          if ($_SESSION['nivel'] == 1) { ?>
           <button type="button" class="btn btn-success mb-3" data-toggle="modal" data-target=".bd-example-modal-xl">Registrar Uso &nbsp; <i class="fas fa-plus"></i></button>
          
          <?php } ?>




<table class="table table-striped text-center">
<thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">USO</th>
      <th scope="col">VER</th>
    </tr>
 </thead>
<tbody id="resultados">

</tbody> 
	
</table>








</div>
<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
          <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Registrar Uso</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
  <form method="POST" action="index.php" enctype="multipart/form-data">
       <div class="modal-body">

  <div class="form-row">
    <div class="form-group col">
      <label for="titulo">Titulo</label>
      <input type="text" class="form-control" name="titulo" placeholder="NOMBRE" required>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col">
      <label for="uso">Uso</label>
      <textarea class="form-control" aria-label="uso" name="uso" required></textarea>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col">
   <label for="inputState">Sutura J&J</label>
      <select id="inputState" class="form-control" name="sutura1">
      <?php 
$query = mysqli_query($enlace,"SELECT * FROM suturas where marca = 1 ");
while ($registro = mysqli_fetch_array($query)) {
  echo ' 
        <option value="'.$registro['id_sutura'].'">'.$registro['nombre'].'&nbsp;'.$registro['codigo'].'</option>'  ;
}?>
      </select>
    </div>
    <div class="form-group col">
     <label for="inputState">Sutura Hirsh</label>
      <select id="inputState" class="form-control" name="sutura2">
          <?php 
$query = mysqli_query($enlace,"SELECT * FROM suturas where marca = 2");
while ($registro = mysqli_fetch_array($query)) {
  echo ' 
        <option value="'.$registro['id_sutura'].'">'.$registro['nombre'].'&nbsp;'.$registro['codigo'].'</option>'  ;
}?>
      </select>
  </div>


</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
      </div>
</form>
  
    </div>
  
</div>
</div>



<?php
include("../defaults/menu.php");
 ?>