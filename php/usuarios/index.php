<?php 
session_start();
  if (!isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] != 1 AND $_SESSION['nivel'] !=1) {
        header("location:../");
    exit;
        }
include("../defaults/conexion.php");
include("../defaults/header.php");

if (isset($_REQUEST['submit'])) {
$nombre = $_REQUEST['nombre'];
$apellido = $_REQUEST['apellido'];
$usuario = $_REQUEST['usuario'];
$pass = md5($_REQUEST['pass']);
$nivel = $_REQUEST['nivel'];
	


$query = mysqli_query($enlace,"INSERT INTO usuarios (nombre,apellido,usuario,pass,nivel)VALUES('$nombre','$apellido','$usuario','$pass','$nivel')");
if (!$query) {
    $mensaje = "error de registro: " .mysqli_error($enlace). PHP_EOL;

}else{

 $mensajeS = "USARIO CREADO DE MANERA EXITOSA";
}

	




}

?>

<div class="container detalle border border-secondary rounded" style="margin-top: 10px;">
<center><h1>Buscar usuario </h1></center>
<br>
<div class="input-group mb-3">
  <input type="text" class="form-control" placeholder="Buscar Usuario" aria-label="Buscar Sutura" aria-describedby="button-addon2" id="ibuscar">
  <div class="input-group-append">
    <button class="btn btn-outline-success" type="button" id="bbuscar" onclick="BuscarUsuario();">Buscar</button>
  </div>
</div>
  <div class="alert alert-danger alert-dismissible" id="labelerror" role="alert" hidden="true">
          
  </div>
<?php 
if (isset($mensaje)) {?>
  <div class="alert alert-danger alert-dismissible" role="alert">
                <strong>Error!</strong> 
            <?php 
            echo $mensaje;
            ?>
  </div>
  <?php 
}

 ?>
 <?php 
if (isset($mensajeS)) {?>
  <div class="alert alert-success alert-dismissible" role="alert">
                <strong>Exito!</strong> 
            <?php 
            echo $mensajeS;
            ?>
  </div>
  <?php 
}

 ?>
 
<button type="button" class="btn btn-success mb-3" data-toggle="modal" data-target=".bd-example-modal-xl">Registrar Usuario &nbsp; <i class="fas fa-plus"></i></button>

<table class="table table-striped center text-center">
<thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">NOMBRE COMPLETO</th>
      <th scope="col">USUARIO</th>
      <th scope="col">NIVEL</th>
      <th scope="col">OPCIONES</th>
    </tr>
 </thead>
<tbody id="resultados">

</tbody> 
	
</table>
</div>

<!-- modal registrar -->


<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Registro de Usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="needs-validation" novalidate action="index.php" method="POST">
                <div class="modal-body">
<!-- row -->
                    <div class="form-row">
                        <div class="col-md-4 mb-3">
                            <label for="validationCustom01">Nombre</label>
                            <input type="text" class="form-control" id="validationCustom01" name="nombre" required>
                            <div class="valid-feedback" id="msgname">
                                Bien hecho!
                            </div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="validationCustom02">Apellido</label>
                            <input type="text" class="form-control" id="validationCustom02" name="apellido" required>
                            <div class="valid-feedback">
                                Bien hecho!
                            </div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="validationCustomUsername">nombre de usuario</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroupPrepend">@</span>
                                </div>
                                <input type="text" class="form-control" id="validationCustomUsername" aria-describedby="inputGroupPrepend" name="usuario" required>
                                <div class="valid-feedback">
                                    !Nombre de usuario valido.
                                </div>
                                <div class="invalid-feedback" id="msguser">

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- row -->
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label for="validationCustom03">contraseña</label>
                            <input type="password" class="form-control" id="validationCustom03" name="pass" minlength="6" required>
                            <div class="invalid-feedback">
                                Debe ingresar una contraseña *Minimo de 6 caracteres
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="validationCustom05">validar contraseña</label>
                            <input type="password" class="form-control" id="validationCustom05" minlength="6" required>
                            <div class="invalid-feedback">
                                Contraseña no coinciden.
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom04">Tipo de usuario</label>
                            <select class="custom-select" id="validationCustom04" name="nivel" required>
                                <option selected disabled value="">Elegir...</option>
                                <option value="1">Administrador</option>
                                <option value="2">usuario</option>
                            </select>
                            <div class="invalid-feedback">
                                Elija un tipo de usuario valido.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" name="submit" id="guardar" type="submit">Guardar</button>
                </div>

            </form>

        </div>

    </div>
</div>

<?php
include("../defaults/menu.php");
 ?>
